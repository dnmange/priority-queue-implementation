public class ApplicationConstants {

    // error and exception messages
    public static final String INVALID_INPUT = "invalid input";
    public static final String UNITS_RANGE_MSG = "units should be between 0 to 150";
    public static final String GPA_RANGE_MSG = "gpa should be between 0.0 to 4.0";

}
