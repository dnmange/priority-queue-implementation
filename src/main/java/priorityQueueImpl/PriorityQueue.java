package priorityQueueImpl;

import java.io.Serializable;
import java.util.*;

public class PriorityQueue<E> extends AbstractQueue<E> implements Serializable {

    private List<Object> queue;
    private final Comparator<? super E> comparator;

    // keeps the count of changes made to the queue
    private int changeCounter = 0;

    public PriorityQueue(Comparator<? super E> comparator){
        this.comparator = comparator;
        queue = new ArrayList<>();
    }

    public PriorityQueue() {
        this((Comparator<? super E>) null);
    }

    // intialize elements from collections
    public PriorityQueue(Collection<? extends E> c) {
        if (c instanceof SortedSet<?>) {
            SortedSet<? extends E> ss = (SortedSet<? extends E>) c;
            this.comparator = (Comparator<? super E>) ss.comparator();
            initializeFromCollection(ss);
        }
        else if (c instanceof java.util.PriorityQueue<?>) {
            java.util.PriorityQueue<? extends E> pq = (java.util.PriorityQueue<? extends E>) c;
            this.comparator = (Comparator<? super E>) pq.comparator();
            initializeFromCollection(pq);
        }
        else {
            this.comparator = null;
            initializeFromCollection(c);
        }
    }

    public PriorityQueue(SortedSet<? extends E> c) {
        this.comparator = (Comparator<? super E>) c.comparator();
        initializeFromCollection(c);
    }

    public PriorityQueue(java.util.PriorityQueue<? extends E> c) {
        this.comparator = (Comparator<? super E>) c.comparator();
        initializeFromCollection(c);
    }

    // after initializing queue is heapify acc to comparator
    private void initializeFromCollection(Collection<? extends E> collection) {
        this.queue = new ArrayList<>(collection);
        heapify();
    }

    private void heapify(){
        for (int i=this.size()/2; i>=0; i--){
            heapifyStateDown(i);
        }
    }
    /*
    * Element is added at end of the queue,
    * this function utilize bottom up approach to find max student priority
    * and place it to the root of queue. maxHeap approach is used here.
    * */
    private void heapifyStateUp(int n){
        if (comparator == null) {
            heapifyStateUpComparable(n);
            return;
        }
        heapifyStateUpUsingComparator(n);
    }

    /*
     * Uses Object compareTo method to determine ordering
     * */
    private void heapifyStateUpComparable(int n) {

        if (n < 0)
            return;

        Comparable<? super E> currentNode = (Comparable<? super E>) queue.get(n);

        int left = 2*n+1;
        int right = 2*n+2;

        if (left < queue.size() && currentNode.compareTo((E) queue.get(left)) > 0){
            swap(left, n);
            heapifyStateUpComparable((n - 1) / 2);
        } else if (right < queue.size() && currentNode.compareTo((E) queue.get(right)) > 0){
            swap(right,n);
            heapifyStateUpComparable((n-1)/2);
        }
    }

    /*
     * Uses user provided comparator compare method to determine ordering
     * */
    private void heapifyStateUpUsingComparator(int n) {

        if (n < 0)
            return;

        int left = 2*n+1;
        int right = 2*n+2;

        if (left < queue.size() && comparator.compare((E) queue.get(n), (E) queue.get(left)) > 0) {
            swap(left,n);
            heapifyStateUpUsingComparator((n-1)/2);
        } else if (right < queue.size() && comparator.compare((E) queue.get(n), (E) queue.get(right)) > 0){
            swap(right,n);
            heapifyStateUpUsingComparator((n-1)/2);
        }
    }

    /*
     * Element at root is removed and placed at end of the queue,
     * this function utilizes top down approach to find next max priority student and place it root
     * and this method is also based on creating maxHeap.
     * */
    private void heapifyStateDown(int n){

        if (comparator == null){
            heapifyStateDownComparable(n);
        } else {
            heapifyStateDownUsingComparator(n);
        }
    }

    /*
    * Uses Object compareTo method to determine ordering
    * */
    private void heapifyStateDownComparable(int n){

        if (n >= queue.size())
            return;

        Comparable<? super E> currentNode = (Comparable<? super E>) queue.get(n);

        int left = 2*n+1;
        int right = 2*n+2;

        int largest = n;

        if (left < queue.size() && currentNode.compareTo((E) queue.get(left)) > 0){
            largest = left;
            currentNode = (Comparable<? super E>) queue.get(largest);
        }

        if (right < queue.size() && currentNode.compareTo((E) queue.get(right)) > 0){
            largest = right;
        }

        if (n != largest){
            swap(largest,n);
            heapifyStateDownComparable(largest);
        }
    }

    /*
     * Uses user provided comparator compare method to determine ordering
     * */
    private void heapifyStateDownUsingComparator(int n){

        if (n >= queue.size())
            return;

        int left = 2*n+1;
        int right = 2*n+2;

        int largest = n;

        if (left < queue.size() && comparator.compare((E) queue.get(n), (E) queue.get(left)) > 0){
            largest = left;
        }

        if (right < queue.size() && comparator.compare((E) queue.get(largest), (E) queue.get(right)) > 0){
            largest = right;
        }

        if (n != largest){
            swap(largest,n);
            heapifyStateDownUsingComparator(largest);
        }
    }

    private void swap(int index1, int index2){

        E object = (E)queue.get(index1);

        queue.set(index1,queue.get(index2));
        queue.set(index2,object);
    }

    public boolean add(E element){
        return offer(element);
    }

    @Override
    public boolean offer(E element) {
        if(element == null)
            throw new NullPointerException();

        queue.add(element);
        changeCounter++;

        heapifyStateUp(queue.size()/2-1);
        return true;
    }

    @Override
    public E remove() {
        return poll();
    }

    @Override
    public E poll() {
        changeCounter++;
        return this.removeAt(0);
    }

    private E removeAt(int index) {

        if(queue.size()==0 || index >= queue.size())
            return null;

        swap(index, queue.size()-1);
        E element = (E) queue.remove(queue.size()-1);
        heapifyStateDown(index);

        return element;
    }

    /*
    * Removes specific element from the queue if present
    * */
    public boolean remove(Object o){

        int index = indexOf(o);
        if (index == -1) return false;

        changeCounter++;
        removeAt(index);
        return true;
    }

    public int indexOf(Object o){
        if (o != null){
            for (int i=0; i<queue.size(); i++){
                if(o.equals(queue.get(i))) {
                    return i;
                }
            }
        }
        return -1;
    }

    /*
    * Returns sorted list of element in toString format
    * */
    public String display() {

        List<E> queueCopy = new ArrayList<E>((Collection<? extends E>) queue);
        List<E> result = new ArrayList<>();

        while(!queue.isEmpty()){
            E element = this.poll();
            result.add(element);
        }

        queue = (List<Object>) queueCopy;
        return result.toString();
    }

    public int size(){
        return queue.size();
    }

    public E peek(){
        return (E) queue.get(0);
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

    private final class Itr<E> implements Iterator<E>{

        int cursor = 0;
        private int expectedChangeCounter = changeCounter; // used to determine concurrent modification

        @Override
        public boolean hasNext() {
            return cursor < queue.size();
        }

        @Override
        public E next() {
            if (expectedChangeCounter != changeCounter)
                throw new ConcurrentModificationException();
            if (cursor < queue.size())
                return (E) queue.get(cursor++);
            throw new NoSuchElementException();
        }

    }

    @Override
    public Object[] toArray() {
        return queue.toArray();
    }

    /*
    * returns type specific array with the same length as of parameter array
    * */
    public <T> T[] toArray(T[] a) {
        return queue.toArray(a);
    }

    @Override
    public String toString(){
        return queue.toString();
    }

}

