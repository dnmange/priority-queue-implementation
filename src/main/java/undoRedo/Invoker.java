package undoRedo;

import priorityQueueImpl.PriorityQueue;
import undoRedo.command.ICommand;

import java.util.ArrayList;
import java.util.List;

public class Invoker<E> {

    private List<ICommand> commandHistory;
    private PriorityQueue<E> queue;

    // to traverse list acc to undo and redo operation
    private int cursor = -1;

    // ignore commands after this validSize
    private int validSize = -1;

    public Invoker(PriorityQueue<E> queue) {
        this.queue = queue;
        commandHistory = new ArrayList<>();
    }


    private void addCommandToHistory(ICommand command)  {

        cursor++;
        commandHistory.add(cursor,command);

        validSize = cursor;
    }

    public boolean undo() {
        if (cursor <= -1)
            return false;

        commandHistory.get(cursor).unexecute(queue);
        cursor--;
        return true;
    }

    public boolean redo() {
        if (cursor >= validSize)
            return false;

        cursor++;
        commandHistory.get(cursor).execute(queue);
        return true;
    }

    public void execute(ICommand command){
        command.execute(queue);
        addCommandToHistory(command);
    }
}
