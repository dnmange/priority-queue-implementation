package undoRedo.command;

import priorityQueueImpl.PriorityQueue;

import java.util.ArrayList;
import java.util.List;


public class RemoveCommandImpl<E> implements ICommand {

    private List<E> elementsToRemove;
    private boolean[] present;


    public RemoveCommandImpl(List<E> elements){
        elementsToRemove = new ArrayList<>(elements);
        present = new boolean[elementsToRemove.size()];
    }

    public RemoveCommandImpl(E element){
        elementsToRemove = new ArrayList<>();
        elementsToRemove.add(element);
        present = new boolean[1];
    }

    /*
     *  Remove elements from the queue that were passed in the constructor
     */
    @Override
    public void execute(PriorityQueue queue) {
        int i=0;
        for(E element: elementsToRemove) {
            if (queue.remove(element))
                present[i++] = true;
        }
    }

    /*
     * Add elements from the queue if removed through this command.
     */
    @Override
    public void unexecute(PriorityQueue queue) {
        int i=0;
        for (E elememt: elementsToRemove){
            if (present[i++])
                queue.add(elememt);
        }
    }
}
