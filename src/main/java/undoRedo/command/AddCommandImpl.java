package undoRedo.command;

import priorityQueueImpl.PriorityQueue;

import java.util.ArrayList;
import java.util.List;

public class AddCommandImpl<E> implements ICommand {

    private List<E> elementsToAdd;

    private AddCommandImpl(){
        elementsToAdd = new ArrayList<>();
    }
    public AddCommandImpl(List<E> elements){
        elementsToAdd = new ArrayList<>(elements);
    }

    public AddCommandImpl(E element){
        this();
        elementsToAdd.add(element);
    }

    /*
     *  Add elements to the queue that were passed in the constructor
     */
    @Override
    public void execute(PriorityQueue queue) {

        for (E elememt: elementsToAdd){
            queue.add(elememt);
        }
    }

    /*
     * Remove elements from the queue if added through this command.
     */
    @Override
    public void unexecute(PriorityQueue queue) {

        for(E element: elementsToAdd) {
            queue.remove(element);
        }
    }
}
