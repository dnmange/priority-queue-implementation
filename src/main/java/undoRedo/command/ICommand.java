package undoRedo.command;

import priorityQueueImpl.PriorityQueue;


public interface ICommand<E> {

    void execute(PriorityQueue<E> queue);
    void unexecute(PriorityQueue<E> queue);
}
