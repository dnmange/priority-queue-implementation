public class Student implements Comparable<Student>{

    private String name;
    private int redId;
    private String email;
    private double gpa;
    private int units;
    private double priority;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRedId() {
        return redId;
    }

    public void setRedId(int redId) {
        this.redId = redId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) throws Exception {
        if (gpa >= 0.0 && gpa <=4.0){
            this.gpa = gpa;
        }else {
            throw new Exception(ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.GPA_RANGE_MSG);
        }
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) throws Exception {
        if (units >= 0 && units <= 150){
            this.units = units;
        }else {
            throw new Exception(ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG);
        }

    }

    public Double getPriority(){
        return this.priority = this.units*0.7/150 + this.gpa*0.3/4;
    }


    @Override
    public int compareTo(Student student) {
        if (this.priority > student.getPriority()) return -1;
        if (this.priority < student.getPriority()) return 1;
        return 0;
    }

    @Override
    public String toString(){
        return "Name: "+this.name+" RedId: "+this.getRedId()+" Email: "+this.getEmail()+" GPA: "+this.getGpa()+" Units: "+this.getUnits()+" Priority: "+this.getPriority();
    }

}
