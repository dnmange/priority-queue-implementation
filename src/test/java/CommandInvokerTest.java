import org.junit.jupiter.api.Test;
import priorityQueueImpl.PriorityQueue;
import undoRedo.Invoker;
import undoRedo.command.AddCommandImpl;
import undoRedo.command.RemoveCommandImpl;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class CommandInvokerTest {

    @Test
    void testUndoRedo() {


        PriorityQueue<Integer> queue = new PriorityQueue();
        Invoker invoker = new Invoker(queue);

        invoker.execute(new AddCommandImpl(Arrays.asList(1,2,3,4)));
        invoker.execute(new RemoveCommandImpl(4));
        invoker.execute(new RemoveCommandImpl(3));

        assertTrue(invoker.undo());
        assertEquals(3, queue.size());

        invoker.execute(new AddCommandImpl(Arrays.asList(5,7)));

        assertFalse(invoker.redo());
        invoker.execute(new AddCommandImpl(Arrays.asList(1,2,3,4)));

        invoker.undo();
        assertEquals(5, queue.size());

        invoker.undo();
        assertEquals(3, queue.size());

        invoker.redo();

        assertEquals(5, queue.size());

        invoker.redo();
        assertEquals(9, queue.size());

        invoker.execute(new AddCommandImpl(10));
        assertFalse(invoker.redo());

        invoker.undo();
        assertEquals(9, queue.size());

        invoker.execute(new RemoveCommandImpl(Arrays.asList(1,2)));
        assertEquals(7, queue.size());
    }

    @Test
    void testUndoRedoEdgeCase(){

        PriorityQueue<Integer> queue = new PriorityQueue();
        Invoker invoker = new Invoker(queue);

        invoker.execute(new AddCommandImpl(Arrays.asList(1,2,3,4)));

        assertTrue(invoker.undo());
        assertEquals(0, queue.size());
        assertFalse(invoker.undo());

        assertTrue(invoker.redo());
        assertEquals(4, queue.size());
        assertFalse(invoker.redo());

    }


}
