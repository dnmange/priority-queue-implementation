import org.junit.jupiter.api.Test;

import java.util.*;
import priorityQueueImpl.PriorityQueue;

import static org.junit.jupiter.api.Assertions.*;


class PriorityQueueTest {

    /*
    * Below test checks the add, remove and display functionality together
    * with respect to inbuilt priority queue
    * */
    @Test
    void testAddRemoveStudent() {

        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        testAdd(expectedQueue, actualQueue, 7);
        testRemove(expectedQueue, actualQueue, 5);
        testAdd(expectedQueue,actualQueue, 5);
    }

    @Test
    void testAddEqualPriorityStudent() throws Exception{
        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        Student student1 = createRandomNewStudent();
        student1.setUnits(100);
        student1.setGpa(3.2);

        Student student2 = createRandomNewStudent();
        student2.setUnits(100);
        student2.setGpa(3.2);

        expectedQueue.add(student1);
        actualQueue.add(student1);

        assertEquals(expectedQueue.peek().getRedId(),actualQueue.peek().getRedId());

        expectedQueue.add(student2);
        actualQueue.add(student2);

        assertEquals(expectedQueue.peek().getRedId(),actualQueue.peek().getRedId());
    }

    private void testAdd(Queue<Student> expectedQueue, PriorityQueue<Student> actualQueue, int noOfStudents) {

        while (noOfStudents-- > 0){
            Student student = createRandomNewStudent();
            actualQueue.add(student);
            expectedQueue.add(student);

            assertEquals(expectedQueue.size(), actualQueue.size());
            assertEquals(expectedQueue.peek().getRedId(),actualQueue.peek().getRedId());
        }
    }

    private void testAdd(Queue<Student> queue, int noOfStudents) {

        int counter = noOfStudents;
        while (counter-- > 0){
            Student student = createRandomNewStudent();
            queue.add(student);
        }
        assertEquals(queue.size(), noOfStudents);
    }

    private void testRemove(Queue<Student> expectedQueue, PriorityQueue<Student> actualQueue, int noOfStudents) {

        while (noOfStudents-- > 0){
            actualQueue.remove();
            expectedQueue.remove();

            assertEquals(expectedQueue.size(), actualQueue.size());

            if(noOfStudents != 0){

                assertEquals(expectedQueue.peek().getRedId(),actualQueue.peek().getRedId());
            }
        }
    }

    @Test
    void testRemoveAt() {
        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        testAdd(expectedQueue,actualQueue,7);
        Student student = createRandomNewStudent();

        actualQueue.add(student);
        expectedQueue.add(student);

        actualQueue.remove(student);
        expectedQueue.remove(student);

        testRemove(expectedQueue, actualQueue, actualQueue.size());
    }

    @Test
    void testDisplay() {

        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        testAdd(expectedQueue, actualQueue, 7);

        Student[] students = expectedQueue.toArray(new Student[7]);
        Arrays.sort(students);

        assertEquals(Arrays.asList(students).toString(), actualQueue.display());
    }

    @Test()
    void testUpperRangeGPA(){

        try{
            Student student = new Student();
            student.setGpa(5.0);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.GPA_RANGE_MSG;
            assertEquals(e.getMessage(),actualMessage);
        }
    }

    @Test()
    void testLowerRangeGPA(){

        try{
            Student student = new Student();
            student.setGpa(-1.0);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.GPA_RANGE_MSG;
            assertEquals(e.getMessage(),actualMessage);
        }
    }

    @Test()
    void testUpperRangeUnits(){

        try{
            Student student = new Student();
            student.setUnits(200);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG;
            assertEquals(e.getMessage(),actualMessage);
        }
    }

    @Test()
    void testLowerRangeUnits(){

        try{
            Student student = new Student();
            student.setUnits(-1);
            fail();
        }catch (Exception e){
            String actualMessage = ApplicationConstants.INVALID_INPUT + " " + ApplicationConstants.UNITS_RANGE_MSG;
            assertEquals(e.getMessage(),actualMessage);
        }
    }

    @Test()
    void testEmptyQueue(){

        PriorityQueue queue = new PriorityQueue();
        assertNull(queue.poll());
    }

    @Test
    void testIterator(){

        PriorityQueue<Student> actualQueue = new PriorityQueue<>();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        int i = 5;
        while (i-- > 0) {
            actualQueue.add(createRandomNewStudent());
        }

        for (Student student : actualQueue) {
            expectedQueue.add(student);
            assertEquals(expectedQueue.peek().getRedId(), actualQueue.peek().getRedId());
        }
    }

    @Test
    void testItrConcurrentModificationExceptionPoll() {
        PriorityQueue<Student> actualQueue = new PriorityQueue<>();

        testAdd(actualQueue, 5);

        try {
            for (Student student: actualQueue) {
                actualQueue.poll();
            }

        } catch (ConcurrentModificationException e){
        }

    }

    @Test
    void testItrConcurrentModificationExceptionAdd() {
        PriorityQueue<Student> actualQueue = new PriorityQueue<>();

        testAdd(actualQueue, 5);

        try {
            for (Student student: actualQueue) {
                actualQueue.add(createRandomNewStudent());
            }

        } catch (ConcurrentModificationException e){
        }

    }

    @Test
    void testItrExceptionRemoveElement() {
        PriorityQueue<Student> actualQueue = new PriorityQueue<>();

        testAdd(actualQueue, 5);
        Student randomNewStudent = createRandomNewStudent();
        actualQueue.add(randomNewStudent);

        try {
            for (Student student: actualQueue) {
                actualQueue.remove(randomNewStudent);
            }

        } catch (ConcurrentModificationException e){
        }

    }

    @Test
    void testToArrayStringStudent(){
        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        testAdd(expectedQueue,  actualQueue, 7);

        Object[] actualArr = actualQueue.toArray();
        Object[] expectedArr = expectedQueue.toArray();

        for (int i=0; i<actualArr.length; i++){
            assertEquals((Student)expectedArr[i],(Student) actualArr[i]);
        }

        assertEquals(expectedQueue.toString(),actualQueue.toString());
    }

    @Test
    void testAddRemoveUsingComparator(){
        Comparator<Student> nameComparator = Comparator.comparing(Student::getName);

        PriorityQueue<Student> actualQueue = new PriorityQueue(nameComparator);
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>(nameComparator);

        testAdd(expectedQueue,  actualQueue, 7);
        testRemove(expectedQueue, actualQueue, 7);
    }

    @Test
    void testToArray(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>();

        for (int i=0; i<10; i++){
            actualQueue.add(i);
            expectedQueue.add(i);
        }

        Object[] actualArr = actualQueue.toArray();
        Object[] expectedArr = expectedQueue.toArray();

        for (int i=0; i<actualArr.length; i++){
            assertEquals((int)expectedArr[i],(int)actualArr[i]);
        }

        assertEquals(expectedQueue.toString(),actualQueue.toString());
    }

    @Test
    void testToArrayWithParam(){
        PriorityQueue<Student> actualQueue = new PriorityQueue();
        Queue<Student> expectedQueue = new java.util.PriorityQueue<>();

        testAdd(expectedQueue, actualQueue, 7);

        Student[] actualArr = actualQueue.toArray(new Student[5]);
        Student[] expectedArr = expectedQueue.toArray(new Student[5]);

        for (int i=0;i<5; i++){
            assertEquals(expectedArr[i].toString(), actualArr[i].toString());
        }
    }

    @Test
    void testToString(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>();

        for (int i=0; i<10; i++){
            actualQueue.add(i);
            expectedQueue.add(i);
        }

        assertEquals(expectedQueue.toString(),actualQueue.toString());
    }

    @Test
    void testOffer(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>();

        for (int i=0; i<10; i++){
            actualQueue.offer(i);
            expectedQueue.offer(i);

            assertEquals(expectedQueue.peek(), actualQueue.peek());
        }
    }

    @Test
    void testOfferException(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();
        assertThrows(NullPointerException.class, ()->{actualQueue.offer(null);});

    }

    @Test
    void testIndexOf(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();
        actualQueue.add(2);

        assertEquals(-1, actualQueue.indexOf(5));
    }

    @Test
    void testNextNoSychElementException(){
        PriorityQueue<Integer> actualQueue = new PriorityQueue();

        Iterator itr = actualQueue.iterator();
        assertThrows(NoSuchElementException.class, ()->itr.next());
    }

    @Test
    void testListCollection(){
        List<Integer> expectedList = new ArrayList<>();
        for (int i=0; i< 10; i++){
            expectedList.add(getRandomNumber());
        }

        PriorityQueue<Integer> actualQueue = new PriorityQueue<>(expectedList);
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>(expectedList);

        assertEquals(expectedQueue.toString(), actualQueue.toString());
    }

    @Test
    void testSortedSetCollection(){
        SortedSet<Integer> sortedSet = new TreeSet<>();
        Collection<Integer> elementCollection = new TreeSet<>();

        for (int i=0; i< 10; i++){
            sortedSet.add(getRandomNumber());
            elementCollection.add(getRandomNumber());
        }

        PriorityQueue<Integer> actualQueue = new PriorityQueue<>(sortedSet);
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>(sortedSet);

        assertEquals(expectedQueue.toString(), actualQueue.toString());

        actualQueue = new PriorityQueue<>(elementCollection);
        expectedQueue = new java.util.PriorityQueue<>(elementCollection);

        assertEquals(expectedQueue.toString(), actualQueue.toString());
    }

    @Test
    void testPriorityQueueCollection(){
        Queue<Integer> elementsCollection = new java.util.PriorityQueue<>();
        java.util.PriorityQueue<Integer> queue = new java.util.PriorityQueue<>();
        for (int i=0; i< 10; i++){
            elementsCollection.add(getRandomNumber());
            queue.add(getRandomNumber());
        }

        PriorityQueue<Integer> actualQueue = new PriorityQueue<>(elementsCollection);
        Queue<Integer> expectedQueue = new java.util.PriorityQueue<>(elementsCollection);

        assertEquals(expectedQueue.toString(), actualQueue.toString());

        actualQueue = new PriorityQueue<>(queue);
        expectedQueue = new java.util.PriorityQueue<>(queue);

        assertEquals(expectedQueue.toString(), actualQueue.toString());
    }

    private Student createRandomNewStudent(){

        Student student = new Student();

        try{
            student.setName(getRandomString());
            student.setRedId(getRandomNumber());
            student.setEmail(getRandomString());
            student.setGpa(getRandomGPA());
            student.setUnits(getRandomUnits());
        }catch (Exception e){
            System.out.print(e.getMessage());
        }

        return student;
    }

    private String getRandomString(){

        String range = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder name = new StringBuilder();
        Random random = new Random();

        for (int i=0;i<6;i++){
            int index = (int) (random.nextFloat() * range.length());
            name.append(range.charAt(index));
        }

        return name.toString();
    }

    private double getRandomGPA(){
        return 1.0 + Math.random() * (3);
    }

    private int getRandomNumber(){
        return  (int)(8000 + Math.random() * (1000));
    }

    private int getRandomUnits(){
        return (int)(0 + Math.random() * (150));
    }
}
